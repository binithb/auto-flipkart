*** Settings ***
Suite Teardown    close browser
Library           auto_flipkart.FlipkartAutoLibrary
Library           Selenium2Library    15    1
Resource          resource.robot

*** Test Cases ***
valid login
    Open Browser    https://flipkart.com    ${BROWSER}
    Maximize Browser Window
    Capture Page Screenshot
    Login    ${valid_username}    ${valid_password}
