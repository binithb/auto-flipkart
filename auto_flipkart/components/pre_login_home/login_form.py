__author__ = 'binithb'
import os

from testatron import WebComponent
from auto_flipkart import components


class LoginForm(WebComponent):
    def __init__(self):
        json_path = os.path.dirname(components.__file__)
        super(LoginForm, self).__init__(self.__class__, json_path, load=True)

    def login(self, username, password, fail=False):
        self.username.send_keys(username)
        self.password.send_keys(password)
        self.login_button.click()
        if fail:
            #TODO have error messages loaded from json
            self.s2l.wait_until_page_contains("you entered did not match our records", 30)

# login = Login ("pre_login_home.json", "login-span")
# login.login("netsgr8_4us@yahoo.com" , "thisispassword")

