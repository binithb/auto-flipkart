__author__ = 'binithb'
import os

from testatron import WebComponent
from auto_flipkart import components


class LoginSignupLinks(WebComponent):
    def __init__(self):
        json_path = os.path.dirname(components.__file__)
        super(self.__class__, self).__init__(self.__class__, json_path, load=True)

    def open_login_form(self):
        self.login_link.click()

