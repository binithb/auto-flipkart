from testatron import WebComponent
from auto_flipkart import components
import os

class HomeStream(WebComponent):
    def __init__(self):
        json_path = os.path.dirname(components.__file__)
        super(HomeStream, self).__init__(self.__class__, json_path)

    def retweet(self, count):
        for retweet_button in self.retweet_buttons[:count][0::2]:
            retweet_button.click()



