from testatron import WebComponent
from auto_flipkart import components
import os

class RetweetDialog(WebComponent):
    def __init__(self):
        json_path = os.path.dirname(components.__file__)
        super(RetweetDialog, self).__init__(self.__class__, json_path)

    def retweet(self, count):
        self.retweet_button.click()



