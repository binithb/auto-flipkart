from testatron import WebComponent
from auto_flipkart import components
import os

class GlobalActions(WebComponent):
    def __init__(self):
        json_path = os.path.dirname(components.__file__)
        super(GlobalActions, self).__init__(self.__class__, json_path)

    def tweet(self, tweet_message):
        self.tweet_box.send_keys(tweet_message)
        self.component_loader.detect_element("tweet_button", make_visible=True)
        self.objectify("tweet_button", self.component_loader.props)
        self.tweet_button.click()



def check_global_actions(test_input=None):
    GlobalActions()
