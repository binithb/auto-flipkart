from auto_flipkart.components.pre_login_home.login_form import LoginForm
from auto_flipkart.components.pre_login_home.login_signup_links import LoginSignupLinks
from auto_flipkart.components.post_login_home.user_greeting import UserGreeting

__author__ = 'binithb'


class Login(object):

    def login(self, username, password, fail=False):
        LoginSignupLinks().open_login_form()
        LoginForm().login(username, password, fail)
        UserGreeting()
