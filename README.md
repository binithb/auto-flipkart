###Web automation for flipkart

#pre-requisites
1. Firefox
2. git

# auto-flipkart

1. Install [testatron](https://gitlab.com/a-kp/testatron) by

  1.git clone https://gitlab.com/binithb/testatron.git
  
  2.cd testtron
  
  3.python setup.py install

2. Install this package by

  1.git clone https://gitlab.com/binithb/auto-flipkart.git
  
  2.cd auto-flipkart
  
  3.python setup.py install

3. Goto auto_flipkart/tests directory of auto-twitter

4. From command line, run python -m robot.run rf
 


